package com.scriptit.hsl;

/**
 * Created by Pravin on 2/21/2018.
 */

public class Group {
    private String groupId;
    private String GroupName;
    private String Type;

    public Group(){}

    public Group(String groupId, String groupName, String type) {
        this.groupId = groupId;
        GroupName = groupName;
        Type = type;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return GroupName;
    }

    public String getType() {
        return Type;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public void setType(String type) {
        Type = type;
    }
}

