package com.scriptit.hsl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pravin on 2/22/2018.
 */

public class ContactAdapter extends ArrayAdapter<ContactNumbers> {
    private Activity context;
    List<ContactNumbers> StoreContacts;

    public ContactAdapter(Activity context, List<ContactNumbers> StoreContacts) {
        super(context,R.layout.contact_items_listview, StoreContacts);

        this.context = context;
        this.StoreContacts = StoreContacts;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.contact_items_listview, null, true);


        ContactNumbers contacts = StoreContacts.get(position);

        TextView firstName = (TextView) listViewItem.findViewById(R.id.cntName);
        TextView favFood = (TextView) listViewItem.findViewById(R.id.cntNumber);

        firstName.setText((CharSequence) contacts.getNumberName());


        favFood.setText(contacts.getNumber());


        return listViewItem;
    }
}



