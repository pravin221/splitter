package com.scriptit.hsl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pravin on 2/11/2018.
 */

public class UserlistAdapter extends ArrayAdapter<User> {

    private Activity context;
    List<User> userList;

    public UserlistAdapter(Activity context, List<User> userList) {
        super(context,R.layout.listview_layout, userList);

        this.context = context;
        this.userList = userList;

    }

    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.listview_layout, null, true);


        User user = userList.get(position);

        if (user != null) {
            TextView firstName = (TextView) listViewItem.findViewById(R.id.username);
           // TextView lastName = (TextView) listViewItem.findViewById(R.id.userdesc);
            TextView favFood = (TextView) listViewItem.findViewById(R.id.useramt);
            if (firstName != null) {
                firstName.setText(user.getName());
            }
         //  if (lastName != null) {
             //  lastName.setText((user.getDesc()));
           // }

//               if (favFood != null) {
//                favFood.setText("₹ "+(user.getAmount()));
//            }
            int g = user.getAmount();
            if(g == 0){
                favFood.setText("      Settled up");
                favFood.setTextColor(R.color.common_google_signin_btn_text_light);

            }
            else {
                favFood.setText("₹ "+(user.getAmount()));
            }

        }


        return listViewItem;
    }
}
