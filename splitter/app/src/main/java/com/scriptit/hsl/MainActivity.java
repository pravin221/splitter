package com.scriptit.hsl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.content.Intent;
import android.widget.Button;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.logging.Handler;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private SectionPageAdapter mSectionPageAdapter;
    private ViewPager mViewPager;
    FloatingActionButton fab_group,fab_calci,NewExpence;
    Animation FabOpen,FabClose,Rclockwise,RAntiClockwise;
    TextView grouptext,calcitext,AnalyticsText;
    Button addFriend;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    boolean isOpen = false;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Log.d(TAG,"onCreate : Starting.");


        AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.appbar);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);



        addFriend = (Button)findViewById(R.id.addfriendbtn);
       // AnalyticsText = (TextView)findViewById(R.id.analyticstext);
        grouptext = (TextView)findViewById(R.id.creategrouptext);
        calcitext = (TextView)findViewById(R.id.calculatortext);
       NewExpence = (FloatingActionButton)findViewById(R.id.newExpence);
        fab_group = (FloatingActionButton)findViewById(R.id.fab_grop);
        fab_calci = (FloatingActionButton)findViewById(R.id.fab_calci);
        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_open);
        FabClose =AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        Rclockwise = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
        RAntiClockwise = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_anticlockwise);
        mSectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager)findViewById(R.id.container);
        setupViewPager(mViewPager);



        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser()==null){

                    Intent o = new Intent(MainActivity.this,Login_Activity.class);
                    startActivity(o);
                }
            }
        };



        final FloatingActionButton it = (FloatingActionButton) findViewById(R.id.floatingActionButton);


        NewExpence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,action.class);
                startActivity(i);
                fab_group.startAnimation(FabClose);
                NewExpence.startAnimation(FabClose);
                fab_calci.startAnimation(FabClose);
                grouptext.startAnimation(FabClose);
                addFriend.startAnimation(FabOpen);
                // AnalyticsButton.startAnimation(FabClose);
                // AnalyticsText.startAnimation(FabClose);
                calcitext.startAnimation(FabClose);
                addFriend.setClickable(true);
                it.startAnimation(RAntiClockwise);
                fab_group.setClickable(false);
                fab_calci.setClickable(false);
                grouptext.setClickable(false);
                isOpen = false;
            }
        });


        fab_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Group_Activity.class);
                startActivity(intent);
                fab_group.startAnimation(FabClose);
                NewExpence.startAnimation(FabClose);
                fab_calci.startAnimation(FabClose);
                grouptext.startAnimation(FabClose);
                addFriend.startAnimation(FabOpen);
               // AnalyticsButton.startAnimation(FabClose);
               // AnalyticsText.startAnimation(FabClose);
                calcitext.startAnimation(FabClose);
                addFriend.setClickable(true);
                it.startAnimation(RAntiClockwise);
                fab_group.setClickable(false);
                fab_calci.setClickable(false);
                grouptext.setClickable(false);
                isOpen = false;

            }
        });


        it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen){
                   // AnalyticsButton.startAnimation(FabClose);
                    //AnalyticsText.startAnimation(FabClose);
                    fab_group.startAnimation(FabClose);
                    fab_calci.startAnimation(FabClose);
                    NewExpence.startAnimation(FabClose);
                    addFriend.startAnimation(FabOpen);
                    grouptext.startAnimation(FabClose);
                    calcitext.startAnimation(FabClose);
                    it.startAnimation(RAntiClockwise);
                    addFriend.setClickable(true);
                    NewExpence.setClickable(false);
                    fab_group.setClickable(false);
                    fab_calci.setClickable(false);
                    grouptext.setClickable(false);
                    isOpen = false;

                }else {
                   // AnalyticsButton.startAnimation(FabOpen);
                   // AnalyticsText.startAnimation(FabOpen);
                    fab_group.startAnimation(FabOpen);
                    addFriend.startAnimation(FabClose);
                    NewExpence.startAnimation(FabOpen);
                    fab_calci.startAnimation(FabOpen);
                    grouptext.startAnimation(FabOpen);
                    calcitext.startAnimation(FabOpen);
                    it.startAnimation(Rclockwise);
                    addFriend.setClickable(false);
                    NewExpence.setClickable(true);
                    fab_group.setClickable(true);
                    fab_calci.setClickable(true);
                    grouptext.setClickable(true);
                    isOpen = true;
                }


            }
        });

        fab_calci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,calci.class);
                fab_group.startAnimation(FabClose);
                fab_calci.startAnimation(FabClose);
                NewExpence.startAnimation(FabClose);
                addFriend.startAnimation(FabOpen);
                grouptext.startAnimation(FabClose);
                calcitext.startAnimation(FabClose);
                //AnalyticsButton.startAnimation(FabClose);
               // AnalyticsText.startAnimation(FabClose);

                it.startAnimation(RAntiClockwise);
                fab_group.setClickable(false);
                addFriend.setClickable(true);
                fab_calci.setClickable(false);
                grouptext.setClickable(false);
                isOpen = false;

                startActivity(i);
            }
        });

        addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Contact_List.class);
                startActivity(i);
            }
        });



    }
    private void setupViewPager(ViewPager viewPager){

        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());

        adapter.addFragment(new tab1(),"Payable");
        adapter.addFragment(new tab2(),"Receivable");
        adapter.addFragment(new tab3(),"Groups");
        viewPager.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout:
                mAuth.signOut();
                break;


        }




        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        int backButtonCount = 1;
        if(backButtonCount >= 1)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }
}

