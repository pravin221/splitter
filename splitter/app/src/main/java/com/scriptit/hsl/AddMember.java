package com.scriptit.hsl;

/**
 * Created by Pravin on 2/21/2018.
 */

public class AddMember {
    private String MemberId;
    private String MemberName;

    public AddMember(){}

    public AddMember(String memberId, String memberName) {
        this.MemberId = memberId;
        MemberName = memberName;
    }

    public String getMemberId() {
        return MemberId;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberId(String memberId) {
        MemberId = memberId;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }
}
