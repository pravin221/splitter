package com.scriptit.hsl;


import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class action extends AppCompatActivity {
    EditText etFavFood;
    AutoCompleteTextView etFirstName,etLastName;
    Button btnAdd,btnView;
    ListView listView;
    List<User> userList;
    ArrayList<String> etlist;
    ArrayAdapter<String> myadapter1;

   DatabaseReference databaseUser;
    DatabaseReference databaserec;


    String [] NamesofPersons = {

            "Ankur","Atish","Dhiraj","Mayur","Gaurav","Rohit","Sahas","Sachin","Piyush","Paresh","Pravin","Pallavi","Hunsraj"
    };

    String [] Descriptions = {
           "Rent","Movies","Sports","Tea","Lunch","Dinner","Grocery","Drinks","Snacks","Breakfast","Coffee","Gifts","Bills","Bus","Car","Hotel","Taxi","Recharge"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        etlist = new ArrayList<>();

        etlist.add("You");
        myadapter1 = new ArrayAdapter<String>(action.this,android.R.layout.simple_list_item_1,etlist);
        myadapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        etFirstName = (AutoCompleteTextView) findViewById(R.id.etFirstName);
        etLastName = (AutoCompleteTextView) findViewById(R.id.etLastName);
        etFavFood = (EditText) findViewById(R.id.etFavFood);


        etFavFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameofuser = etFirstName.getText().toString();
                etlist.add(""+nameofuser);

                myadapter1.notifyDataSetChanged();
            }
        });


        Spinner myspinner1 = (Spinner)findViewById(R.id.spinneryou);
        myspinner1.setAdapter(myadapter1);








        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnView = (Button) findViewById(R.id.btnView);
        databaseUser = FirebaseDatabase.getInstance().getReference("payable");
        databaserec = FirebaseDatabase.getInstance().getReference("receivable");
        String btnName = etFirstName.getText().toString();

                ArrayAdapter NameAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,NamesofPersons);
                etFirstName.setThreshold(-1);

                etFirstName.setAdapter(NameAdapter);


        ArrayAdapter DescriptionAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,Descriptions);
        etLastName.setThreshold(0);
        etLastName.setAdapter(DescriptionAdapter);





        Spinner myspinner = (Spinner)findViewById(R.id.spinner1);
        ArrayAdapter<String> myadapter = new ArrayAdapter<String>(action.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.names));
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myspinner.setAdapter(myadapter);

        myspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if(selectedItem.equals("Split equally")){

                    btnView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AddDatatoSplittedpay();
                            Intent i = new Intent(action.this,MainActivity.class);
                            startActivity(i);
                        }
                    });
                    btnAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddDataSplittedrec();

                            Intent i = new Intent(action.this,MainActivity.class);
                            startActivity(i);
                        }
                    });

                }
                if(selectedItem.equals("Split None")){

                    btnView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AddDatatopay();
                            Intent i = new Intent(action.this,MainActivity.class);
                            startActivity(i);
                        }
                    });
                    btnAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AddData();

                            Intent i = new Intent(action.this,MainActivity.class);
                            startActivity(i);
                        }
                    });

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                btnView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddDatatopay();
                        Intent i = new Intent(action.this,MainActivity.class);
                        startActivity(i);
                    }
                });
                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AddData();

                        Intent i = new Intent(action.this,MainActivity.class);
                        startActivity(i);
                    }
                });


            }
        });

            }


    public void AddData(){
        String Name = etFirstName.getText().toString();
        String Desc = etLastName.getText().toString();
        int Amount = Integer.parseInt(etFavFood.getText().toString());

        if (!TextUtils.isEmpty(Name)){
            String id = databaserec.push().getKey();

            User us = new User(id,Name,Desc,Amount);

            databaserec.child(id).setValue(us);

            Toast.makeText(this,"Added successfully",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this,"Add something",Toast.LENGTH_SHORT).show();
        }

    }


    public void AddDataSplittedrec(){
        String Name = etFirstName.getText().toString();
        String Desc = etLastName.getText().toString();
        int val = Integer.parseInt(etFavFood.getText().toString());

        int Amount = val/2;

        if (!TextUtils.isEmpty(Name)){
            String id = databaserec.push().getKey();

            User us = new User(id,Name,Desc,Amount);

            databaserec.child(id).setValue(us);

            Toast.makeText(this,"Added successfully",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this,"Add something",Toast.LENGTH_SHORT).show();
        }

    }


    public void AddDatatoSplittedpay(){
        String Name = etFirstName.getText().toString();
        String Desc = etLastName.getText().toString();
        int val = Integer.parseInt(etFavFood.getText().toString());
        int Amount = val/2;

        if (!TextUtils.isEmpty(Name)){
            String id = databaseUser.push().getKey();

            User us = new User(id,Name,Desc,Amount);

            databaseUser.child(id).setValue(us);

            Toast.makeText(this,"Added successfully",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this,"Add something",Toast.LENGTH_SHORT).show();
        }

    }



    public void AddDatatopay(){
        String Name = etFirstName.getText().toString();
        String Desc = etLastName.getText().toString();
        int Amount = Integer.parseInt(etFavFood.getText().toString());

        if (!TextUtils.isEmpty(Name)){
            String id = databaseUser.push().getKey();

            User us = new User(id,Name,Desc,Amount);

            databaseUser.child(id).setValue(us);

            Toast.makeText(this,"Added successfully",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this,"Add something",Toast.LENGTH_SHORT).show();
        }

    }



}
