package com.scriptit.hsl;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pravin on 2/25/2018.
 */

public class tab3 extends Fragment {

    DatabaseReference Groupdatabase;
    List<Group> groupList;
    ListView list;
    public static final String GroupName = "groupName";
    public static final String GroupId = "groupId";
    public static final String GroupType = "groupType";




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Groupdatabase = FirebaseDatabase.getInstance().getReference("Groups");
        groupList = new ArrayList<>();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab3, container, false);
        list = (ListView)view.findViewById(R.id.grplist);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Group group = groupList.get(position);
                Intent intent = new Intent(getActivity(),Showgroups.class);
                intent.putExtra(GroupName,group.getGroupName());
                intent.putExtra(GroupId,group.getGroupId());
                intent.putExtra(GroupType,group.getType());

                startActivity(intent);


            }
        });


        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Groupdatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                groupList.clear();

                for (DataSnapshot groupsnap : dataSnapshot.getChildren()) {
                    Group group = groupsnap.getValue(Group.class);
                    groupList.add(group);
                }
                GroupAdapter adapter = new GroupAdapter(getActivity(),groupList);
                list.setAdapter(adapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
