package com.scriptit.hsl;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Group_Activity extends AppCompatActivity {

    Button AddMemberBtn,createGp;
    EditText GroupNameEt;
    ListView groupmemberslist;
    List<String> namelist;
    Animation FabOpen,FabClose;
    DatabaseReference Groupdatabase;
    DatabaseReference MemberDatabase;
    Spinner myspinner;
    AutoCompleteTextView autoMember;
    TextView txt10;
    ImageButton btn3;
    List<AddMember>memberList;
    public static  String groupName = "groupname";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_);

        FabOpen = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_open);
        FabClose =AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);

        btn3 = (ImageButton)findViewById(R.id.imageButton3);
        memberList = new ArrayList<>();
        txt10 = (TextView)findViewById(R.id.textView10);
        Groupdatabase = FirebaseDatabase.getInstance().getReference("Groups");
        final String id =Groupdatabase.push().getKey();
        MemberDatabase = FirebaseDatabase.getInstance().getReference("members").child(id);

        AddMemberBtn = (Button)findViewById(R.id.memberAddBtn);
        createGp = (Button)findViewById(R.id.create);
        GroupNameEt = (EditText) findViewById(R.id.groupnameET);
        groupmemberslist = (ListView)findViewById(R.id.groupmemberlist);
        final TextView txt = (TextView)findViewById(R.id.txtgrpname);



         myspinner = (Spinner)findViewById(R.id.groupspinner);
        ArrayAdapter<String> myadapter = new ArrayAdapter<String>(Group_Activity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.groupType));
        myadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myspinner.setAdapter(myadapter);


        createGp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupName = GroupNameEt.getText().toString();
                txt.setText(groupName);
                txt.startAnimation(FabOpen);
                String GroupName = GroupNameEt.getText().toString();
                String GroupType = myspinner.getSelectedItem().toString();

                if(!TextUtils.isEmpty(GroupName)){

                    Group group = new Group(id,GroupName,GroupType);
                    Groupdatabase.child(id).setValue(group);
                    createGp.startAnimation(FabClose);
                    GroupNameEt.startAnimation(FabClose);
                    AddMemberBtn.startAnimation(FabOpen);
                    btn3.startAnimation(FabOpen);
                    myspinner.startAnimation(FabClose);
                    txt10.startAnimation(FabOpen);
                }

            }
        });



        AddMemberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                ShowmemberDialog();


            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();

        MemberDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                memberList.clear();

                for(DataSnapshot groupsnp : dataSnapshot.getChildren()){

                    AddMember member = groupsnp.getValue(AddMember.class);
                    memberList.add(member);
                }
                memberlistAdapter adapter = new memberlistAdapter(Group_Activity.this,memberList);
                groupmemberslist.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu groupmenu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.groupsave,groupmenu);
        return super.onCreateOptionsMenu(groupmenu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.savebtn:

                Intent intent = new Intent(Group_Activity.this,MainActivity.class);
                startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }

    private void ShowmemberDialog() {

        AlertDialog.Builder alertdialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.memberadddialog, null);
        alertdialog.setView(dialogView);

        autoMember = (AutoCompleteTextView)dialogView.findViewById(R.id.memberEtauto);
        Button Add = (Button)dialogView.findViewById(R.id.addtolist);
        TextView Gpname = (TextView)dialogView.findViewById(R.id.gpnametxt);
        GroupNameEt = (EditText) findViewById(R.id.groupnameET);
        GroupNameEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               }
        });

        String hiname = GroupNameEt.getText().toString();
        Gpname.setText("Group "+hiname);
        final AlertDialog showdialog = alertdialog.create();
        showdialog.show();


        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addMember();
                showdialog.dismiss();

            }
        });


    }




    private void addMember(){

        String MemberName = autoMember.getText().toString();
        String ID = MemberDatabase.push().getKey();
        AddMember member = new AddMember(ID,MemberName);
        MemberDatabase.child(ID).setValue(member);
        Toast.makeText(this,"Member Added",Toast.LENGTH_LONG).show();

    }


}
