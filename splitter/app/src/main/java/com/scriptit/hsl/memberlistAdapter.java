package com.scriptit.hsl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pravin on 2/22/2018.
 */

public class memberlistAdapter extends ArrayAdapter<AddMember> {
private Activity context;
        List<AddMember> memberList;

public memberlistAdapter(Activity context, List<AddMember> memberList) {
        super(context,R.layout.memberlists, memberList);

        this.context = context;
        this.memberList = memberList;

        }

@NonNull
@Override
public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.memberlists, null, true);


        AddMember member = memberList.get(position);

        TextView firstName = (TextView) listViewItem.findViewById(R.id.names);


        firstName.setText((CharSequence) member.getMemberName());




        return listViewItem;
        }
        }


