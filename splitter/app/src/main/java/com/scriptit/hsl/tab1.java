package com.scriptit.hsl;


import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.os.Build.ID;

/**
 * Created by Pravin on 9/3/2017.
 */

public class tab1 extends Fragment {

    public static final String TAG = "tab1";
    public static final String UserName = "username";
    public static final String UserId = "userid";
    public static final String Userdesc = "userdesc";
    public static final String UserAmount = "useramt";

    DatabaseReference userdatabase;
    List<User> userList;
    ListView list;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userdatabase = FirebaseDatabase.getInstance().getReference("payable");
        userList = new ArrayList<>();






    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1, container, false);
        list = (ListView) view.findViewById(R.id.tab1list);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = userList.get(position);
                Intent i = new Intent(getActivity(), updateact.class);
                i.putExtra(UserName, user.getName());
                i.putExtra(UserId, user.getId());
                i.putExtra(Userdesc, user.getDesc());
                i.putExtra(UserAmount, user.getAmount());
                startActivity(i);
            }
        });


           list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {



                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Delete");
                alert.setMessage("Do you want to delete this item? ");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        User user1 = userList.get(position);
                        final String userItem = user1.getId();
                        DeleteUserId(userItem);


                    }


                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();

                return false;
            }

        });



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        userdatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                userList.clear();

                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    User user = usersnapshot.getValue(User.class);
                    userList.add(user);
                }
                UserlistAdapter adapter = new UserlistAdapter(getActivity(),userList);
                list.setAdapter(adapter);
                String t = String.valueOf(sum());
               TextView tot = (TextView)getActivity().findViewById(R.id.num3);
                tot.setText("₹ "+t);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public int sum(){
        int result = 0;
        for(int i = 0; i < userList.size(); i++){
            result += userList.get(i).getAmount();
        }
        return result;
    }

    private void DeleteUserId(String id){
        DatabaseReference deleteuser = FirebaseDatabase.getInstance().getReference("payable").child(id);
        deleteuser.removeValue();

    }


}

