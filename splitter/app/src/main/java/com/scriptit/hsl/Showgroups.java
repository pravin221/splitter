package com.scriptit.hsl;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.scriptit.hsl.tab3.GroupId;

public class Showgroups extends AppCompatActivity {

    DatabaseReference Groupdatabase;
    List<Group> groupList;
    ListView list;
    TextView GroupName;
    ImageView ImageType;
    List<AddMember> memberList;
    FloatingActionButton Gexpence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showgroups);

        GroupName = (TextView)findViewById(R.id.gpnameshow);
        ImageType = (ImageView)findViewById(R.id.typeImageshow);
        Intent intent = getIntent();
        final String id = intent.getStringExtra(tab3.GroupId);
        final String GroupName1 = intent.getStringExtra(tab3.GroupName);
        final String GroupType = intent.getStringExtra(tab3.GroupType);

        if (GroupType.equals("Apartment")){

            ImageType.setImageResource(R.drawable.home);

        }
        else if (GroupType.equals("Office")){

            ImageType.setImageResource(R.drawable.office);
        }
        else if (GroupType.equals("Trip")){

            ImageType.setImageResource(R.drawable.trip);
        }
        else if (GroupType.equals("Other")){
            ImageType.setImageResource(R.drawable.other);

        }

        GroupName.setText(GroupName1);
        memberList = new ArrayList<>();
        Groupdatabase = FirebaseDatabase.getInstance().getReference("members").child(id);
        groupList = new ArrayList<>();
        list = (ListView)findViewById(R.id.grouplist);

        list.setClickable(false);

        Gexpence = (FloatingActionButton)findViewById(R.id.groupexpence);
        Gexpence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();



        Groupdatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                groupList.clear();

                for (DataSnapshot groupsnap : dataSnapshot.getChildren()) {
                    AddMember member = groupsnap.getValue(AddMember.class);
                    memberList.add(member);


                }
                memberlistAdapter adapter = new memberlistAdapter(Showgroups.this,memberList);
                list.setAdapter(adapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

          }
}
