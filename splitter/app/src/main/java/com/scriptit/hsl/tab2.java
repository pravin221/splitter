package com.scriptit.hsl;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pravin on 9/3/2017.
 */

public class tab2 extends Fragment {
    public static final String TAG = "tab2";

    public static final String UserName2 = "username2";
    public static final String UserId2 = "userid2";
    public static final String Userdesc2 = "userdesc2";
    public static final String UserAmount2 = "useramt2";
    DatabaseReference userdatabase;
    List<User> userList;
    ListView list;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userdatabase = FirebaseDatabase.getInstance().getReference("receivable");
        userList = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2,container,false);

         list = (ListView)view.findViewById(R.id.listview1);

         list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                 User user = userList.get(position);
                 Intent i = new Intent(getActivity(),updatetab2.class);
                 i.putExtra(UserName2,user.getName());
                 i.putExtra(UserId2,user.getId());
                 i.putExtra(Userdesc2,user.getDesc());
                 i.putExtra(UserAmount2,user.getAmount());
                 startActivity(i);
             }
         });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {


                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Delete");
                alert.setMessage("Do you want to delete this item? ");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        User user1 = userList.get(position);
                        final String userItem = user1.getId();
                        DeleteUserId(userItem);

                    }


                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();
                return false;
            }

        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        userdatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                userList.clear();

                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    User user = usersnapshot.getValue(User.class);
                    userList.add(user);
                }
                UserlistAdapter adapter = new UserlistAdapter(getActivity(),userList);
                list.setAdapter(adapter);
                String t = String.valueOf(sum());
                TextView tot = (TextView)getActivity().findViewById(R.id.num4);
                tot.setText("₹ "+t);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public int sum(){
        int result = 0;
        for(int i = 0; i < userList.size(); i++){
            result += userList.get(i).getAmount();
        }
        return result;
    }
    private void DeleteUserId(String id){
        DatabaseReference deleteuser = FirebaseDatabase.getInstance().getReference("receivable").child(id);
        deleteuser.removeValue();

    }

    }
