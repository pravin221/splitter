package com.scriptit.hsl;

/**
 * Created by Pravin on 2/18/2018.
 */

public class UserHistory {
    private String historyId;
    private String HistoryName;
    private int HistoryAmount;

  public UserHistory(){}

    public UserHistory(String historyId, String historyName, int historyAmount) {
        this.historyId = historyId;
        HistoryName = historyName;
        HistoryAmount = historyAmount;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public void setHistoryName(String historyName) {
        HistoryName = historyName;
    }

    public void setHistoryAmount(int historyAmount) {
        HistoryAmount = historyAmount;
    }

    public String getHistoryId() {
        return historyId;
    }

    public String getHistoryName() {
        return HistoryName;
    }

    public int getHistoryAmount() {
        return HistoryAmount;
    }
}
