package com.scriptit.hsl;

/**
 * Created by Pravin on 2/11/2018.
 */

public class User {

    String id;
    String Name;
    String Desc;
    int Amount;

    public User() {

    }


    public User(String id, String name, String desc,int amount) {
        this.id = id;
        Name = name;
        Desc = desc;
        Amount = amount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public String getDesc() {
        return Desc;
    }

    public int getAmount() {
        return Amount;
    }
}

