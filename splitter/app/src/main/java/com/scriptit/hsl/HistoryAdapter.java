package com.scriptit.hsl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pravin on 2/18/2018.
 */

public class HistoryAdapter extends ArrayAdapter <UserHistory> {
    private Activity context;
    List<UserHistory> historyList;

    public HistoryAdapter(Activity context, List<UserHistory> historyList) {
        super(context,R.layout.history_layout, historyList);

        this.context = context;
        this.historyList = historyList;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.history_layout, null, true);


        UserHistory history = historyList.get(position);

            TextView firstName = (TextView) listViewItem.findViewById(R.id.datedetails);
            TextView favFood = (TextView) listViewItem.findViewById(R.id.amountHistory);

               firstName.setText((CharSequence) history.getHistoryName()+" paid");


                favFood.setText("₹ "+String.valueOf(history.getHistoryAmount()));


        return listViewItem;
    }
}

