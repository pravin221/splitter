package com.scriptit.hsl;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class updatetab2 extends AppCompatActivity {
    List<UserHistory> historyList;
    ListView list;
    String id ="hi";
    EditText editamt;
    TextView rcamttext;

    DatabaseReference userHistorydatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatetab2);
        historyList = new ArrayList<>();

        list = (ListView)findViewById(R.id.desclist2);
        rcamttext = (TextView)findViewById(R.id.rcamt2);
        TextView name = (TextView)findViewById(R.id.wlname2);
        Intent intent = getIntent();


        final String Name2 = intent.getStringExtra(tab2.UserName2);
        final int rcamt = intent.getIntExtra(tab2.UserAmount2,0);

        id = intent.getStringExtra(tab2.UserId2);
        name.setText(Name2);
        rcamttext.setText("₹ "+rcamt);
        userHistorydatabase = FirebaseDatabase.getInstance().getReference("UserHistory").child(id);

        Button updatingButton = (Button) findViewById(R.id.updatingbutton2);
        updatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showipdatedialog(id,Name2);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        userHistorydatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                historyList.clear();

                for(DataSnapshot tracksnapshot : dataSnapshot.getChildren()){

                    UserHistory history = tracksnapshot.getValue(UserHistory.class);
                    historyList.add(history);
                }
                HistoryAdapter adapter = new HistoryAdapter(updatetab2.this,historyList);
                list.setAdapter(adapter);
                String t = String.valueOf(sum());
                TextView tot = (TextView)findViewById(R.id.recnum);
                tot.setText("₹ "+t);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showipdatedialog(String userid, String username){

        AlertDialog.Builder alertdialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.updatedialog,null);

        alertdialog.setView(dialogView);

        editamt = (EditText)dialogView.findViewById(R.id.updateamt);
        final TextView OtherName =(TextView)dialogView.findViewById(R.id.textViewother);
        final Button save = (Button)dialogView.findViewById(R.id.updatesave);
        Intent intent = getIntent();
        final String Name = intent.getStringExtra(tab2.UserName2);
        final String Desc = intent.getStringExtra(tab2.Userdesc2);
        final String Id = intent.getStringExtra(tab2.UserId2);



        OtherName.setText(Name);
        alertdialog.setTitle("Receive From "+username);


        final AlertDialog showdialog = alertdialog.create();
        showdialog.show();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                final int Amtval = intent.getIntExtra(tab2.UserAmount2,0);

                final int Amount = Integer.parseInt(editamt.getText().toString());
                int finalAmoint = Amtval - Amount;
                if (finalAmoint <= 0){
                    int positiveval = -finalAmoint;

                    DatabaseReference userupdatereferance = FirebaseDatabase.getInstance().getReference("payable");
                    String id = userupdatereferance.push().getKey();
                    User us = new User(id,Name,Desc,positiveval);
                    userupdatereferance.child(id).setValue(us);
                    DeleteUserId(Id);
                    showdialog.dismiss();
                    Intent i = new Intent(updatetab2.this,MainActivity.class);
                    startActivity(i);

                    Toast.makeText(getApplicationContext(), "Amount is Updated", Toast.LENGTH_LONG).show();


                }else {

                    updateUsertab2(Id, Name, Desc, finalAmoint);
                    updateUserHistory();
                    showdialog.dismiss();
                    Intent r = new Intent(updatetab2.this,MainActivity.class);
                    startActivity(r);
                    Toast.makeText(getApplicationContext(), "Amount is Updated", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    private boolean updateUsertab2(String id,String UserName,String Desc,int Amount){

        DatabaseReference userupdatereferance = FirebaseDatabase.getInstance().getReference("receivable").child(id);

        User user = new User(id,UserName,Desc,Amount);
        userupdatereferance.setValue(user);
        return true;
    }

    private void DeleteUserId(String id){
        DatabaseReference deleteuser = FirebaseDatabase.getInstance().getReference("receivable").child(id);
        deleteuser.removeValue();

    }
    private void updateUserHistory(){
        Intent intent = getIntent();
        final int Amount = Integer.parseInt(editamt.getText().toString());
        String Name = intent.getStringExtra(tab2.UserName2);
        String id = userHistorydatabase.push().getKey();
        UserHistory user = new UserHistory(id,Name,Amount);
        userHistorydatabase.child(id).setValue(user);
    }

    public int sum(){
        int result = 0;
        for(int i = 0; i < historyList.size(); i++){
            result += historyList.get(i).getHistoryAmount();
        }
        return result;
    }

}
