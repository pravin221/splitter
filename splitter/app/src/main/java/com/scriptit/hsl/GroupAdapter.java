package com.scriptit.hsl;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pravin on 2/25/2018.
 */

public class GroupAdapter extends ArrayAdapter<Group> {
private Activity context;
        List<Group> grouplist;

public GroupAdapter(Activity context, List<Group> grouplist) {
        super(context,R.layout.grouplist, grouplist);

        this.context = context;
        this.grouplist = grouplist;

        }

@NonNull
@Override
public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.grouplist, null, true);


        Group member = grouplist.get(position);

        ImageView typeImage = (ImageView)listViewItem.findViewById(R.id.typeImage);
        TextView firstName = (TextView) listViewItem.findViewById(R.id.Name);

         TextView type = (TextView) listViewItem.findViewById(R.id.type);

        firstName.setText((CharSequence) member.getGroupName());
        type.setText(member.getType());
        if (member.getType().equals("Apartment")){

                typeImage.setImageResource(R.drawable.home);

        }
        else if (member.getType().equals("Office")){

                typeImage.setImageResource(R.drawable.office);
        }
        else if (member.getType().equals("Trip")){

                typeImage.setImageResource(R.drawable.trip);
        }
        else if (member.getType().equals("Other")){
                typeImage.setImageResource(R.drawable.other);

        }



        return listViewItem;
        }
        }



