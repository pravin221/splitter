package com.scriptit.hsl;

/**
 * Created by Pravin on 2/22/2018.
 */

public class ContactNumbers {

    private String NumberId;
    private String NumberName;
    private String Number;

    public ContactNumbers(){}

    public ContactNumbers(String numberName,String number) {
        NumberName = numberName;
        Number = number;
    }

    public String getNumberId() {
        return NumberId;
    }

    public String getNumberName() {
        return NumberName;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumberId(String numberId) {
        NumberId = numberId;
    }

    public void setNumberName(String numberName) {
        NumberName = numberName;
    }

    public void setNumber(String number) {
        Number = number;
    }
}
