package com.scriptit.hsl;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

public class updateact extends AppCompatActivity {

    List<UserHistory> historyList;
   DatabaseReference userHistorydatabase;
    ListView list;
    EditText editamt;
    String id ="hi";
    TextView pyamt;
    FloatingActionButton AddMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actupdate);


        AddMore = (FloatingActionButton)findViewById(R.id.addmore);
        list = (ListView)findViewById(R.id.amtlist);
        historyList = new ArrayList<>();
        TextView name = (TextView)findViewById(R.id.wlnamepy);
        Intent intent = getIntent();
        final String Name1 = intent.getStringExtra(tab1.UserName);
        id = intent.getStringExtra(tab1.UserId);
        final int pyamtstring = intent.getIntExtra(tab1.UserAmount,0);
        pyamt = (TextView)findViewById(R.id.pyamtoo);
        pyamt.setText("₹ "+pyamtstring);
       userHistorydatabase = FirebaseDatabase.getInstance().getReference("UserHistory").child(id);

        name.setText(Name1);


        Button updatingButton = (Button)findViewById(R.id.updatingbutton);

        AddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReceiveMoreDialog();
            }
        });

    updatingButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            showipdatedialogtab1(id,Name1);
        }
    });

    }
    @Override
    protected void onStart() {
        super.onStart();
        userHistorydatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                historyList.clear();

                for(DataSnapshot tracksnapshot : dataSnapshot.getChildren()){

                    UserHistory history = tracksnapshot.getValue(UserHistory.class);
                    historyList.add(history);
                }
                HistoryAdapter adapter = new HistoryAdapter(updateact.this,historyList);
                list.setAdapter(adapter);

                String t = String.valueOf(sum());
                TextView tot = (TextView)findViewById(R.id.paidnum);
                tot.setText("₹ "+t);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void ReceiveMoreDialog(){

        AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.addmoredialog,null);

        newDialog.setView(dialogView);

        final EditText RCMoreet = (EditText)dialogView.findViewById(R.id.etReceiveMore);
        Button RCMoreBtn = (Button)dialogView.findViewById(R.id.btRCmore);

        final AlertDialog showdialog1 = newDialog.create();
        showdialog1.show();
        RCMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = getIntent();
                final int userAmount = intent1.getIntExtra(tab1.UserAmount,0);
                final String Name = intent1.getStringExtra(tab1.UserName);
                final String Desc = intent1.getStringExtra(tab1.Userdesc);
                final String Id = intent1.getStringExtra(tab1.UserId);
                int RCM = Integer.parseInt(RCMoreet.getText().toString());

                int FinalAmount = userAmount + RCM;
                updateUserpay(Id,Name,Desc,FinalAmount);
                Intent i = new Intent(updateact.this,MainActivity.class);
                startActivity(i);
            showdialog1.dismiss();

            }
        });


    }




    private void showipdatedialogtab1(String userid, String username){

        AlertDialog.Builder alertdialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.updatedialog,null);

        alertdialog.setView(dialogView);

         editamt = (EditText)dialogView.findViewById(R.id.updateamt);
        final TextView OtherName =(TextView)dialogView.findViewById(R.id.textViewother);
        final Button save = (Button)dialogView.findViewById(R.id.updatesave);
        Intent intent = getIntent();
        final String Name = intent.getStringExtra(tab1.UserName);
        final String Desc = intent.getStringExtra(tab1.Userdesc);
        final String Id = intent.getStringExtra(tab1.UserId);



        OtherName.setText(Name);
        alertdialog.setTitle("Pay To "+username);


        final AlertDialog showdialog = alertdialog.create();
        showdialog.show();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                final int Amtval = intent.getIntExtra(tab1.UserAmount,0);

                final int Amount = Integer.parseInt(editamt.getText().toString());
                int finalAmoint = Amtval - Amount;

                if (finalAmoint <= 0){
                    int positiveval = -finalAmoint;
                    DatabaseReference userupdatereferance = FirebaseDatabase.getInstance().getReference("receivable");
                    String id = userupdatereferance.push().getKey();
                    User us = new User(id,Name,Desc,positiveval);
                    userupdatereferance.child(id).setValue(us);
                    DeleteUserId(Id);
                    showdialog.dismiss();
                    Intent i = new Intent(updateact.this,MainActivity.class);
                    startActivity(i);
                    Toast.makeText(getApplicationContext(), "Amount is Updated", Toast.LENGTH_LONG).show();


                }else {


                    updateUserpay(Id, Name, Desc, finalAmoint);
                    updateUserHistory();
                    showdialog.dismiss();
                    Intent intent1 = new Intent(updateact.this,MainActivity.class);
                    startActivity(intent1);
                    Toast.makeText(getApplicationContext(), "Amount is Updated", Toast.LENGTH_LONG).show();

                }
            }
        });

    }




    private boolean updateUserpay(String id,String UserName,String Desc,int Amount){

        DatabaseReference userupdatereferance = FirebaseDatabase.getInstance().getReference("payable").child(id);

        User user = new User(id,UserName,Desc,Amount);
        userupdatereferance.setValue(user);
        return true;
    }

    private void updateUserHistory(){
        Intent intent = getIntent();
        final int Amount = Integer.parseInt(editamt.getText().toString());
        String Name = intent.getStringExtra(tab1.UserName);
        String id = userHistorydatabase.push().getKey();
        UserHistory user = new UserHistory(id,Name,Amount);
        userHistorydatabase.child(id).setValue(user);
    }


    private void DeleteUserId(String id){
        DatabaseReference deleteuser = FirebaseDatabase.getInstance().getReference("payable").child(id);
        deleteuser.removeValue();

    }

    public int sum(){
        int result = 0;
        for(int i = 0; i < historyList.size(); i++){
            result += historyList.get(i).getHistoryAmount();
        }
        return result;
    }

}
